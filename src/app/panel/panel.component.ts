import { AfterViewInit, Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'ok-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.scss']
})
export class PanelComponent implements OnInit, AfterViewInit {

  @ViewChild('panel') panel: ElementRef;
  @ViewChild('container') container: ElementRef;
  ctx: CanvasRenderingContext2D;

  panelSize: {width: number, height: number} = {
    width: 100,
    height: 100
  };
  defSize = 2;
  defColor = '#555';
  moveFlg: boolean;

  xPoint: number;
  yPoint: number;
  constructor() { }

  ngOnInit() {
    this.resize();
  }
  ngAfterViewInit() {
    const canvas = this.panel.nativeElement;
    this.ctx = canvas.getContext('2d');
  }
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.resize();
  }

  startPoint(e) {
    e.preventDefault();
    this.ctx.beginPath();

    this.xPoint = e.layerX;
    this.yPoint = e.layerY;

    this.ctx.moveTo(this.xPoint, this.yPoint);
  }

  movePoint(e) {
    if (e.buttons === 1 || e.witch === 1 || e.type === 'touchmove') {

      this.xPoint = e.layerX;
      this.yPoint = e.layerY;
      this.moveFlg = true;

      this.ctx.lineTo(this.xPoint, this.yPoint);
      this.ctx.lineCap = 'round';
      this.ctx.lineWidth = this.defSize;
      this.ctx.strokeStyle = this.defColor;
      this.ctx.stroke();

    }
  }

  endPoint(e) {

    if (!this.moveFlg) {
      this.ctx.lineTo(this.xPoint - 1, this.yPoint - 1);
      this.ctx.lineCap = 'round';
      this.ctx.lineWidth = this.defSize * 2;
      this.ctx.strokeStyle = this.defColor;
      this.ctx.stroke();

    }
    this.moveFlg = false;
  }
  resize() {
    const containerNe = this.container.nativeElement;
    this.panelSize.width = containerNe.clientWidth;
    this.panelSize.height = containerNe.clientHeight;
  }
}
